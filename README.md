# Generic Datatable Component

## Overview

A generic Aura Component that we can use to view object records. To use it we pass the object name, fields we want to display, and the number of records per page. We can sort on columns, select rows, and see a popup with the selected rows information.

## Demo (~50 sec)

[Here's a link to a demo video for the component](https://www.youtube.com/watch?v=R6LzdMhETK4)

## Some Screenshots

### Datatable Example

![Datatable Example](media/datatable.png)

### Viewing Selected Rows

![Viewing Selected Rows](media/selected-records-popup.png)
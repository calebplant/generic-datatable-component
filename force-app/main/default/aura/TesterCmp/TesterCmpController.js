({
    doInit : function(component, event, helper) {
        console.log('GenericDatatable :: doInit');
        component.set("v.objectName", "Account");
        let fields = [
            {label: 'Name', fieldName: 'Name', type: 'text', sortable: true},
            {label: 'Industry', fieldName: 'Industry', type: 'text', sortable: true},
            {label: 'Shipping Country', fieldName: 'ShippingCountry', type: 'text', sortable: true},
            {label: 'Shipping State', fieldName: 'ShippingState', type: 'text', sortable: true},
            {label: 'Annnual Revenue', fieldName: 'AnnualRevenue', type: 'text', sortable: true}
        ];
        component.set("v.displayedFields", fields);

        let table = component.find("recordTable");
        table.set("v.selectedRows", ['0010R000016DWFcQAO']);

        let passedFields = component.get("v.displayedFields").map(f => f.fieldName);

        let action = component.get("c.getObjectRecords");
        action.setParams({objectName: component.get("v.objectName"), fields: passedFields});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                console.log('Response from server: ');
                console.log(response.getReturnValue());
                component.set("v.records", response.getReturnValue());
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    },

    handleRowSelect : function(component, event, helper) {
        console.log('TesterCmp :: handleRowSelect');
    },

    handleClick : function(component, event, helper) {
        console.log('TesterCmp :: handleClick');
        // component.set("v.userSelectedRows", []);
        component.set("v.userSelectedRows", ["0010R000016DWFiQAO"]);
        
    },

})

({
    doInit : function(component, event, helper) {
        console.log('GenericDatatable :: doInit');
        component.set("v.objectName", "Account");
        let fields = [
            {label: 'Name', fieldName: 'Name', type: 'text', sortable: true},
            {label: 'Industry', fieldName: 'Industry', type: 'text', sortable: true},
            {label: 'Shipping Country', fieldName: 'ShippingCountry', type: 'text', sortable: true},
            // {label: 'Shipping State', fieldName: 'ShippingState', type: 'text', sortable: true},
            {label: 'Ownership', fieldName: 'Ownership', type: 'text', sortable: true},
            {label: 'Annnual Revenue', fieldName: 'AnnualRevenue', type: 'number', sortable: true}
        ];
        component.set("v.displayedFields", fields);

        let table = component.find("recordTable");
        table.set("v.selectedRows", ['0010R000016DWFcQAO']);

        let passedFields = component.get("v.displayedFields").map(f => f.fieldName);

        let action = component.get("c.getObjectRecords");
        action.setParams({objectName: component.get("v.objectName"), fields: passedFields});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                console.log('Response from server: ');
                console.log(response.getReturnValue());
                component.set("v.records", response.getReturnValue());
                helper.updatePagination(component, response.getReturnValue());
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    },

    handlePrevious : function(component, event, helper) {
        console.log('GenericDatatable :: handlePrevious');
        let pageNumber = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber", pageNumber - 1);
        component.set("v.userSelectedRows", []);
        // component.set("v.isChangingPage", "true");
        helper.updateDisplayedData(component);
    },

    handleNext : function(component, event, helper) {
        console.log('GenericDatatable :: handleNext');
        let pageNumber = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber", pageNumber + 1);
        component.set("v.userSelectedRows", []);
        // component.set("v.isChangingPage", "true");
        helper.updateDisplayedData(component);
    },

    handleRowSelect : function(component, event, helper) {
        console.log('GenericDatatable :: handleRowSelect');

        var currentRows = component.get("v.selectedRowsByPage");
        currentRows[component.get("v.currentPageNumber")] = event.getParam("selectedRows").map(r => r.Id);
        // currentRows[component.get("v.currentPageNumber")] = event.getParam("selectedRows");
        console.log('current rows:');
        console.log(JSON.parse(JSON.stringify(currentRows)));
        component.set("v.selectedRowsByPage", currentRows);
        // component.set("v.selectedRowsByPage", currentRows.map(r => r.Id));
    },

    handleSort : function(component, event, helper) {
        console.log('GenericDatatable :: handleSort');
        helper.handleSort(component, event);
    },

    handleShowSelected : function(component, event, helper) {
        console.log('GenericDatatable :: handleShowSelected');
        let selectedRowIds = helper.getSelectedRowIds(component);
        let modalRecords = component.get("v.records").filter(eachRecord => selectedRowIds.includes(eachRecord.Id));
        console.log('Modal records: ');
        console.log(JSON.parse(JSON.stringify(modalRecords)));
        component.set("v.modalRecords", modalRecords);
        helper.openModal(component);
    },

    handleCloseSelected : function(component, event, helper) {
        console.log('GenericDatatable :: handleCloseSelected');
        helper.closeModal(component);
    },

    handleDebug : function(component, event, helper) {
        console.log('HANDLE DEBUG');
        component.set("v.userSelectedRows", []);
    },


})

({
    sortBy: function(field, reverse, primer) {
        var key = primer
            ? function(x) {
                  return primer(x[field]);
              }
            : function(x) {
                  return x[field];
              };

        return function(a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    },

    handleSort: function(component, event) {
        console.log('GenericDatatableHelper :: handleSort');
        var sortedBy = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');

        var cloneData = component.get("v.records").slice(0);
        cloneData.sort((this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1)));
        
        component.set('v.records', cloneData);
        component.set('v.sortDirection', sortDirection);
        component.set('v.sortedBy', sortedBy);
        component.set("v.currentPageNumber", 1);
        let selectedRowIds = this.getSelectedRowIds(component);
        this.updatePagination(component, cloneData);
        this.updateSelectedRowsAfterSort(component, cloneData, selectedRowIds);
    },

    updateDisplayedData : function(component) {
        console.log('GenericDatatableHelper :: updateDisplayedData');
        let data = [];
        let pageNumber = component.get("v.currentPageNumber");
        let pageSize = component.get("v.pageSize");
        let records = component.get('v.records');
        let x = (pageNumber - 1) * pageSize;
        for (; x < (pageNumber) * pageSize; x++){
            if (records[x]) {
                data.push(records[x]);
            }
        }
        component.set("v.displayedRecords", data);
        this.setSelectedRows(component);
    },

    setSelectedRows : function(component) {
        console.log('GenericDatatableHelper :: setSelectedRows');

        let pageNumber = component.get("v.currentPageNumber");
        let storedSelections = component.get("v.selectedRowsByPage");

        console.log(JSON.parse(JSON.stringify(storedSelections)));
        console.log('Page number: ' + pageNumber);
        console.log(JSON.parse(JSON.stringify(storedSelections[pageNumber])));
        // let selectedRows = storedSelections[pageNumber].map(r => r.Id);
        let selectedRows = storedSelections[pageNumber];
        console.log('result: ');
        try {
            console.log(JSON.parse(JSON.stringify(selectedRows)));

        } catch (err) {
            console.log(err);
        }
        // component.set("v.userSelectedRows", storedSelections[pageNumber].map(r => r.Id));
        component.set("v.userSelectedRows", selectedRows);
    },

    updatePagination : function(component, records) {
        console.log('GenericDatatableHelper :: updatePagination');
        let countTotalPage = Math.ceil(records.length/component.get("v.pageSize"));
        let totalPage = countTotalPage > 0 ? countTotalPage : 1;
        component.set("v.totalPages", totalPage);
        component.set("v.currentPageNumber", 1);
        let selectedRowsByPage = {};
        for(let i=1; i <= totalPage; i++) {
            selectedRowsByPage[i] = [];
        }
        component.set("v.selectedRowsByPage", selectedRowsByPage, selectedRowsByPage);
        this.updateDisplayedData(component);
    },

    updateSelectedRowsAfterSort : function(component, sortedRecords, selectedRowIds) {
        console.log('GenericDatatableHelper :: updateSelectedRowsAfterSort');

        try {
            let pageSize = component.get("v.pageSize");
            let totalPages = component.get("v.totalPages");
    
            let selectedRowsByPage = {};
            for(let i=1; i <= totalPages; i++) {
                selectedRowsByPage[i] = [];
            }

            console.log('Selected records: ');
            console.log(JSON.parse(JSON.stringify(selectedRowIds)));
    
            for(let i=0; i < sortedRecords.length; i++) {
                console.log(sortedRecords[i]);
                if(selectedRowIds.includes(sortedRecords[i].Id)) {
                    let page = Math.floor(i / pageSize) + 1;
                    selectedRowsByPage[page].push(sortedRecords[i].Id);
                }
            }
    
            console.log('RESULT:');
            console.log(JSON.parse(JSON.stringify(selectedRowsByPage)));
            component.set("v.selectedRowsByPage", selectedRowsByPage);
            component.set("v.userSelectedRows", selectedRowsByPage[1]);
        } catch(err) {
            console.error(err);
        }
    },

    getSelectedRowIds : function(component) {
        console.log('GenericDatatableHelper :: getSelectedRowIds');
        let selectedRowsByPage = component.get("v.selectedRowsByPage");
        let selectedRows = [];
        for (const [key, value] of Object.entries(selectedRowsByPage)) {
            // value.forEach(row => selectedRows.push(row.Id));
            value.forEach(row => selectedRows.push(row));
        }
        console.log('Result:');
        console.log(JSON.parse(JSON.stringify(selectedRows)));
        return selectedRows;
    },

    openModal: function(component) {
        component.set("v.isModalOpen", true);
     },
    
     closeModal: function(component) {
        component.set("v.isModalOpen", false);
     },
})

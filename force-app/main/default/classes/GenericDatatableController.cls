public with sharing class GenericDatatableController {
    
    @AuraEnabled(cacheable=true)
    public static List<SObject> getObjectRecords(String objectName, List<String> fields) {
        System.debug('START getObjectRecords');
        
        if(objectName != null && fields != null) {
            String query = 'SELECT ' + String.join(fields, ',') + ' FROM ' + String.escapeSingleQuotes(objectName) + ' LIMIT 2000';
            System.debug(query);
            List<SObject> objs = Database.query(query);
            objs.sort();
            return objs;
        }
        return null;
    }

    public static Object getRandVal(List<Object> values) {
        Integer randomNumber = Integer.valueof((Math.random() * values.size()-1));
        return values[randomNumber];
    }

    public static Integer getRandInt(Integer max) {
        return Integer.valueof((Math.random() * max));
    }
}
